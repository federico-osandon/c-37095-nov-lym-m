
import { Link } from "react-router-dom"
// import { ContextApp } from "../../App"

const Item = ({ product }) => {
   
    return (
        <div            
            style={{ marginLeft: 100}}
            className='col-md-3'
            key={product.id}
        >   
            <Link to={`/detail/${product.id}`} >
                
                <div className="card w-100 mt-5" >
                    <div className="card-header">
                        {`${product.name} - ${product.categoria}`}
                    </div>
                    <div className="card-body">
                        <img src={product.foto} alt='' className='w-50' />
                        {product.price}                                                            
                    </div>
                    {/* <button onClick={saludar} >Saludar</button> */}
                </div>

            </Link>
                            
            
        </div>  
    )
}

export default Item